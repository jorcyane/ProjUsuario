import business.control.ControlVeiculos;
import business.control.VeiculosDisponiveisFacade;
import business.model.Veiculo;
import exception.InfraException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 * @author Jorcyane
 */
public class testeFacade extends TestCase {
    
    public testeFacade(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testVeiculosDisponiveis(){
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        Map<String,Veiculo> veiculos= new HashMap<>();
        
        String veiculosDisponiveis="";
        veiculos = controlveiculos.getAllVeiculos();
        Iterator todasveiculos;
        Veiculo veiculo;
        
        if (!veiculos.isEmpty()){
            todasveiculos = veiculos.values().iterator();
            while (todasveiculos.hasNext()) {
                veiculo = (Veiculo)todasveiculos.next();
                if (veiculo.getDisponivel()){
                    veiculosDisponiveis = veiculosDisponiveis + "["+veiculo.toString()+"]" + "\n";
                }
            }
        }
        
        assertEquals(veiculosDisponiveis,VeiculosDisponiveisFacade.listarVeiculosDisponiveis());
    }
    
    public void testCriarLocarVeiculo(){
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        assertEquals(null,controlveiculos.buscaVeiculo("nometestefaçade"));
        try {
            assertEquals("Veículo locado com sucesso",VeiculosDisponiveisFacade.criarVeiculoLocar("nometestefaçade"));
        } catch (InfraException ex) {
            
        }
        
    }
    
    public void testRemoveVeiculo(){
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        try {
            assertTrue(controlveiculos.removeVeiculo("nometestefaçade"));
        } catch (InfraException ex) {
        }
        
        
    }
}
