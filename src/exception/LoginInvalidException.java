package exception;
/**
 *
 * @author Jorcyane
 */
public class LoginInvalidException extends Exception{
    public LoginInvalidException(){
    	super("Login invalido");
    }
   
    public LoginInvalidException(String Message){
        super(Message);
    }
}
