package exception;

/**
 *
 * @author Jorcyane
 */
public class VeiculoErrorException extends Exception{
    public VeiculoErrorException(){
    	super("Erro - Tente novamente!");
    }
   
    public VeiculoErrorException(String Message){
        super(Message);
    }
}
