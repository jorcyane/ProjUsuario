package exception;

/**
 *
 * @author Jorcyane
 */
public class PasswordInvalidException extends Exception {
    public PasswordInvalidException(){
    	super("Login invalido");
    }
   
    public PasswordInvalidException(String Message){
        super(Message);
    }
}
