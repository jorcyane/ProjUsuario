package exception;
/**
 *
 * @author Jorcyane
 */
public class InfraException extends Exception{
    public InfraException(){
    	super("Erro de persistencia, contacte o admin ou tente mais tarde.");
    }
    public InfraException(String msg){        
        super(msg);
    }
}
