package mock;

import javax.swing.JOptionPane;

/**
 *
 * @author Jorcyane
 */
public class SimulacaoRedesSociais {
    
    public static void publicarRedesSociais(String divulgacao, String usuarioAlvo){
        JOptionPane.showMessageDialog(null, "Promoção "+divulgacao +" publicada com sucesso para o usuário "+usuarioAlvo);
    }
}
