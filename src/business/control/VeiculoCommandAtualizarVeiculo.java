package business.control;

import business.model.Veiculo;
import exception.InfraException;
import exception.VeiculoErrorException;
import view.GuiVeiculo;

/**
 *
 * @author Arthuci e Jorcyane
 */
public class VeiculoCommandAtualizarVeiculo implements VeiculoCommandIF{
//Usando o padrão Command 
    @Override
    public void executarAcao() {
        String nome,nome_new,descricao,marca,modelo, resposta;
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        nome = GuiVeiculo.entradaDados("Nome do veículo:");
        Veiculo veiculo;
        veiculo = controlveiculos.buscaVeiculo(nome);
        
        if (veiculo!=null){
            GuiVeiculo.saidaDados(veiculo.toString());
            resposta = GuiVeiculo.saidaOptions("Atualizar veículo?");
            if (resposta.equals("YES")){
                nome_new = GuiVeiculo.entradaDados("Novo Nome do veículo:");
                descricao = GuiVeiculo.entradaDados("Nova Descricao do veículo:");
                marca = GuiVeiculo.entradaDados("Nova Marca do veículo:");
                modelo = GuiVeiculo.entradaDados("Novo Modelo do veículo:");
                
                try {
                    controlveiculos.atualizar(nome, nome_new, descricao, marca, modelo, veiculo.getDisponivel());
                } catch (InfraException ex) {
                    GuiVeiculo.saidaDados(ex.getMessage());
                } catch (VeiculoErrorException ex) {
                    GuiVeiculo.saidaDados(ex.getMessage());
                }
                GuiVeiculo.saidaDados("Veículo atualizado com sucesso:\n"+veiculo.toString());
            }
        }else{
            GuiVeiculo.saidaDados("Veículo não encontrado");
        }
    }
    
}
