package business.control;

import business.model.Promocao;
import exception.InfraException;
import infra.FabricaFileIF;
import infra.FabricaPromocaoFile;
import infra.PromocaoFile;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Jorcyane
 */
public class GerenciaPromocoes {
    private Map<String,Promocao> promocoes= new HashMap<>();
    PromocaoFile promocaoFile;
	
    public GerenciaPromocoes() {		
		
         /*usando o padrão factory method */	
        FabricaFileIF fabrica=new FabricaPromocaoFile();
        promocaoFile = (PromocaoFile)fabrica.criarFile();
        try {
            promocoes = promocaoFile.load();
        } catch (InfraException e) {
            System.out.println(e.getMessage());
        } 
		
    }
    
    public void addPromocao(String nome, String descricao) throws InfraException {

        promocoes.put(nome,new Promocao(nome,descricao));
        promocaoFile.save(promocoes);
    }
        
    public Map<String,Promocao> getAllPromocoes() {
        return promocoes;
    }
    
    public String getAllPromocoesString(){
        String promocoesstring = "";
        Iterator todaspromocoes;
        Promocao promocao;
        if (!promocoes.isEmpty()){
            todaspromocoes = promocoes.values().iterator();
            while (todaspromocoes.hasNext()) {
                promocao = (Promocao)todaspromocoes.next();
                promocoesstring = promocoesstring + "["+promocao.toString()+"]" + "\n";
            }
            return promocoesstring;
        }else {
            return ("Nenhuma promocao cadastrada!" );
        }

    }
    
    public Promocao buscaPromocao(String nome){
        return promocoes.get(nome);
    }
    
    public boolean removePromocao(String nome) throws InfraException{
        if (buscaPromocao(nome)!=null){
            promocoes.remove(nome);
            promocaoFile.remove(promocoes);
            
            return true;
         
        }else {
            return false;
        }
    }
    
    public boolean alteraState(String nome)throws InfraException{
        Promocao promocao=buscaPromocao(nome); 
        if (promocao!=null){
            promocao.alteraState();
            return true;
        }else {
            return false;
        }
    }
}
