package business.control;

import business.model.User;
import business.model.UserAdm;
import business.model.ValidacaoUser;
import exception.InfraException;
import exception.LoginInvalidException;
import exception.PasswordInvalidException;
import infra.FabricaFileIF;
import infra.FabricaUserFile;
import infra.UserFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Jorcyane
 */
public class ControlUser {
    private Map<String,User> usuarios= new HashMap<>();
    UserFile userFile;
	
    public ControlUser() {		
	/*usando o padrão factory method */	
        FabricaFileIF fabrica=new FabricaUserFile();
        userFile = (UserFile)fabrica.criarFile();
        try {
            usuarios = userFile.load();
        } catch (InfraException e) {
            System.out.println(e.getMessage());
        } 
		
    }
    
    public void addUser(String nome, String password) throws LoginInvalidException, PasswordInvalidException, InfraException {

        ValidacaoUser.validateName(nome);
        ValidacaoUser.validatePassword(password);

        usuarios.put(nome,new User(nome,password));
        userFile.save(usuarios);
    }
    
    public void addUserAdm(String nome, String password) throws LoginInvalidException, PasswordInvalidException, InfraException {

        ValidacaoUser.validateName(nome);
        ValidacaoUser.validatePassword(password);

        usuarios.put(nome,new UserAdm(nome,password));
        userFile.save(usuarios);
    }
    
    public Map<String,User> getAllClients() {
        return usuarios;
    }
    
    public String getAllClientsString(){
        String usuariosstring = "";
        Iterator todosusuarios;
        User user;
        if (!usuarios.isEmpty()){
            todosusuarios = usuarios.values().iterator();
            while (todosusuarios.hasNext()) {
                user = (User)todosusuarios.next();
                usuariosstring = usuariosstring + "["+user.toString()+"]" + "\n";
            }
            return usuariosstring;
        }else {
            return ("Nenhum usuário cadastrado!" );
        }

    }
    
    public User buscaUsuario(String nome){
        return usuarios.get(nome);
    }
    
    public boolean removeUsuario(String nome) throws InfraException{
        if (buscaUsuario(nome)!=null){
            usuarios.remove(nome);
            userFile.remove(usuarios);
            
            return true;
         
        }else {
            return false;
        }
    }
    
    public ArrayList<String> getListAllNames(){
        ArrayList<String> nomeusuariosstring=new ArrayList<>();
        Iterator todosusuarios;
        User user;
        if (!usuarios.isEmpty()){
            todosusuarios = usuarios.values().iterator();
            while (todosusuarios.hasNext()) {
                user = (User)todosusuarios.next();
                nomeusuariosstring.add(user.getLogin());
            }
                
        }
        return nomeusuariosstring;
    }
    
}
