package business.control;

/**
 *
 * @author Jorcyane
 */
public class RelatorioPromocoes extends GerenciaRelatoriosTemplate{
/*usando o padrão template method */
    @Override
    public String cabecalhoespecifico() {
        String cabecalhoEspecifico="Promoções Cadastrados";
        return cabecalhoEspecifico;
    }

    @Override
    public String corpoespecifico() {
       GerenciaPromocoes gerenciapromocoes=new GerenciaPromocoes();
       String corpoEspecifico="";
       corpoEspecifico+=gerenciapromocoes.getAllPromocoesString();
       return corpoEspecifico;
    }
    
}
