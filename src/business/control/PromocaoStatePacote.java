package business.control;

import java.io.Serializable;

/**
 *
 * @author  Jorcyane
 */
public class PromocaoStatePacote implements PromocaoStateIF,Serializable{
    //Padrão State
    @Override
    public PromocaoStateIF alteraState() {
        return new PromocaoStateBasico();
    }
    
    @Override
    public String imprimeState(){
        return "Promoção Pacote";
    }
    

}
