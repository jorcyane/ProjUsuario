package business.control;

/**
 *
 * @author Jorcyane
 */
public class RelatorioUsuario extends GerenciaRelatoriosTemplate{
/*usando o padrão template method */    
    @Override
    public String cabecalhoespecifico(){
        String cabecalhoEspecifico="Usuários Cadastrados";
        return cabecalhoEspecifico;
    }

    @Override
    public String corpoespecifico() {
        ControlUser controluser=new ControlUser();
        String corpoEspecifico="";
        corpoEspecifico+=controluser.getAllClientsString();
        return corpoEspecifico;
    }
    
   

}
