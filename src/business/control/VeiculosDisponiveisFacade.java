package business.control;

import business.model.Veiculo;
import exception.InfraException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Jorcyane
 */
public class VeiculosDisponiveisFacade {
    /*usando o padrão Facade */
    public static String listarVeiculosDisponiveis(){
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        Map<String,Veiculo> veiculos= new HashMap<>();
        
        String veiculosDisponiveis="";
        veiculos = controlveiculos.getAllVeiculos();
        Iterator todasveiculos;
        Veiculo veiculo;
        
        if (!veiculos.isEmpty()){
            todasveiculos = veiculos.values().iterator();
            while (todasveiculos.hasNext()) {
                veiculo = (Veiculo)todasveiculos.next();
                if (veiculo.getDisponivel()){
                    veiculosDisponiveis = veiculosDisponiveis + "["+veiculo.toString()+"]" + "\n";
                }
            }
            if (!veiculosDisponiveis.equals("")){
               return veiculosDisponiveis;
            }else {
                return ("Nenhum veiculo disponível!" );
            }
            
        }else {
            return ("Nenhum veiculo cadastrado!" );
        }
        
    }
    public static String criarVeiculoLocar(String nomeVeiculo) throws InfraException{
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        Veiculo veiculo;
        veiculo = controlveiculos.buscaVeiculo(nomeVeiculo);
        if (veiculo==null){
            controlveiculos.addVeiculo(nomeVeiculo, "veículo Façade", "GM", "Celta");
            veiculo = controlveiculos.buscaVeiculo(nomeVeiculo);
        }
        
        System.out.println(veiculo.getNomeCarro()); 
        if (veiculo!=null){
            if (controlveiculos.locar(nomeVeiculo))return "Veículo locado com sucesso";
            else return "Erro na locação";
        }
        
        return "";
        
    }
}
