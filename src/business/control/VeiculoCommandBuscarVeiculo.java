package business.control;

import business.model.Veiculo;
import view.GuiVeiculo;

/**
 *
 * @author Jorcyane
 */
public class VeiculoCommandBuscarVeiculo implements VeiculoCommandIF{
//Usando o padrão Command 
    @Override
    public void executarAcao() {
        String nome;
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        nome = GuiVeiculo.entradaDados("Nome do veículo:");
        Veiculo veiculo;
        
        veiculo = controlveiculos.buscaVeiculo(nome);
        
        if (veiculo!=null){
            GuiVeiculo.saidaDados(veiculo.toString());
        }else{
            GuiVeiculo.saidaDados("Veículo não encontrado");
        }
    }
    
}
