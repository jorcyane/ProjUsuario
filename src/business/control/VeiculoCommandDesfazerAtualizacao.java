package business.control;

import business.model.Veiculo;
import exception.InfraException;
import exception.VeiculoErrorException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import view.GuiVeiculo;

/**
 *
 * @author Jorcyane
 */
public class VeiculoCommandDesfazerAtualizacao implements VeiculoCommandIF{

    @Override
    public void executarAcao() {
        String nome,resposta;
        
         //usando o padrão singleton
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        nome = GuiVeiculo.entradaDados("Desfazer Alterações \nNome do veículo:");
        Veiculo veiculo;
        veiculo = controlveiculos.buscaVeiculo(nome);
        if (veiculo!=null){
            GuiVeiculo.saidaDados(veiculo.toString());
            resposta = GuiVeiculo.saidaOptions("Desfazer Alterações?");
            if (resposta.equals("YES")){
                try {
                    controlveiculos.desfazerAtualizacao(veiculo);
                } catch (VeiculoErrorException ex) {
                    GuiVeiculo.saidaDados(ex.getMessage());
                } catch (InfraException ex) {
                    GuiVeiculo.saidaDados(ex.getMessage());
                }
                
            }
        } else{
            GuiVeiculo.saidaDados("Veículo não encontrado");
        }
    }
    
}
