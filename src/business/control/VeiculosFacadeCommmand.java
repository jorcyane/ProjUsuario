package business.control;

/**
 *
 * @author Jorcyane
 */
public class VeiculosFacadeCommmand {
//usando o  padrão Facade e o Command 
    VeiculoCommandIF command;
    public void addVeiculos(){
        command = new VeiculoCommandAddVeiculo();
        executar();
    }
    
    public void buscaVeiculos(){
        command = new VeiculoCommandBuscarVeiculo();
        executar();
    }
    
    public void atualizaVeiculos(){
        command = new VeiculoCommandAtualizarVeiculo();
        executar();
    }
    
    public void desfazerAtualizacao(){
        command = new VeiculoCommandDesfazerAtualizacao();
        executar();
        
    }
    
    public void executar(){
        command.executarAcao();
    }
    
}
