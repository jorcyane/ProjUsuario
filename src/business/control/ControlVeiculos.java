package business.control;

import business.model.Veiculo;
import business.model.memento.MementoVeiculo;
import business.model.memento.VeiculoCareTaker;
import exception.InfraException;
import exception.VeiculoErrorException;
import infra.FabricaFileIF;
import infra.FabricaVeiculoFile;
import infra.VeiculoFile;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jorcyane
 */
public class ControlVeiculos {
    private Map<String,Veiculo> veiculos= new HashMap<>();
    VeiculoFile veiculoFile;
    VeiculoCareTaker veiculoCareTaker = new VeiculoCareTaker(); 
     //usando o padrão singleton
    static ControlVeiculos instanciaControlVeiculos=null;
    
    private ControlVeiculos() {		
		
        /*usando o padrão factory method */	
        FabricaFileIF fabrica=new FabricaVeiculoFile();
        veiculoFile = (VeiculoFile)fabrica.criarFile();
        try {
            veiculos = veiculoFile.load();
        } catch (InfraException e) {
            System.out.println(e.getMessage());
        } 
		
    }
    //usando o padrão singleton
    public static ControlVeiculos getInstanciaCV(){
        if (instanciaControlVeiculos==null)
            instanciaControlVeiculos=new ControlVeiculos();
        return instanciaControlVeiculos;
    }
    
    
    public void addVeiculo(String nome, String descricao,String marca,String modelo) throws InfraException {

        veiculos.put(nome,new Veiculo(nome,descricao,marca,modelo));
        veiculoFile.save(veiculos);
    }
        
    public Map<String,Veiculo> getAllVeiculos() {
        return veiculos;
    }
    
    public String getAllVeiculosString(){
        String veiculosstring = "";
        Iterator todasveiculos;
        Veiculo veiculo;
        try {
            veiculos = veiculoFile.load();
        } catch (InfraException ex) {
            Logger.getLogger(ControlVeiculos.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (!veiculos.isEmpty()){
            todasveiculos = veiculos.values().iterator();
            while (todasveiculos.hasNext()) {
                veiculo = (Veiculo)todasveiculos.next();
                veiculosstring = veiculosstring + "["+veiculo.toString()+"]" + "\n";
            }
            return veiculosstring;
        }else {
            return ("Nenhum veiculo cadastrado!" );
        }

    }
    
    public Veiculo buscaVeiculo(String nome){
        return veiculos.get(nome);
    }
    
    public boolean removeVeiculo(String nome) throws InfraException{
        if (buscaVeiculo(nome)!=null){
            veiculos.remove(nome);
            veiculoFile.remove(veiculos);
            
            return true;
         
        }else {
            return false;
        }
    }
    
    public boolean locar(String nome) throws InfraException {
        boolean msg;
        Veiculo veiculo=buscaVeiculo(nome);
        if (veiculo!=null){
            if (veiculo.getDisponivel()){
                veiculo.setDisponivel(false);
                veiculoFile.edita(veiculos);
                msg = true;
            }else{
                msg = false;
            }
           
        }else {
            msg = false;
        }
        
        return msg;
    }
    
    public String devolver(String nome) throws InfraException {
        String msg;
        Veiculo veiculo=buscaVeiculo(nome);
        if (veiculo!=null){
            if (!veiculo.getDisponivel()){
                veiculo.setDisponivel(true);
                veiculoFile.edita(veiculos);
                msg = "Devolução registrada com sucesso";
                
            }else{
                msg = "Erro - Veículo não estava locado.";
            }
           
        }else {
            msg = "Erro - Veículo não encontrado.";
        }
        
        return msg;
    }
    
    public void atualizar(String nome_old,String nome_new,String descricao,String marca,String modelo,boolean disponivel) throws InfraException,VeiculoErrorException {
        
        Veiculo veiculo=buscaVeiculo(nome_old);
        if (veiculo!=null){
            MementoVeiculo memento = new MementoVeiculo(nome_old,veiculo.getDescricao(),veiculo.getMarca(),veiculo.getModelo(),veiculo.getDisponivel());
            veiculoCareTaker.adicionarMemento(memento);
            veiculo.setNomeCarro(nome_new);
            veiculo.setDescricao(descricao);
            veiculo.setMarca(marca);
            veiculo.setModelo(modelo);
            veiculo.setDisponivel(disponivel);
            veiculos.remove(nome_old);
            veiculos.put(veiculo.getNomeCarro(),veiculo);
            veiculoFile.edita(veiculos);
            
        }else {
            throw new VeiculoErrorException("Erro - Veículo não encontrado.");
        }     
        
    }
    
    //Padrão memento
    public void desfazerAtualizacao(Veiculo veiculo) throws VeiculoErrorException, InfraException{
        MementoVeiculo memento = veiculoCareTaker.getUltimoEstadoSalvo();
        if (memento==null){
            throw new VeiculoErrorException("Erro - Nenhuma ação a ser desfeita!");
        }else{
            atualizar(veiculo.getNomeCarro(),memento.getNome(),memento.getDescricao(),memento.getMarca(),memento.getModelo(),memento.getDisponivel());
        }
        
    }
}
