package business.control;

import exception.InfraException;
import view.GuiVeiculo;

/**
 *
 * @author Jorcyane
 */
public class VeiculoCommandAddVeiculo implements VeiculoCommandIF{

//Usando o padrão Command 
    @Override
    public void executarAcao() {
        String nome,descricao,marca,modelo,msg;
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        nome = GuiVeiculo.entradaDados("Nome do veículo:");
        descricao = GuiVeiculo.entradaDados("Descricao do veículo:");
        marca = GuiVeiculo.entradaDados("Marca do veículo:");
        modelo = GuiVeiculo.entradaDados("Modelo do veículo:");
                    
        try {
            controlveiculos.addVeiculo(nome, descricao, marca, modelo);
            GuiVeiculo.saidaDados("Veículo adicionado com sucesso!");
        } catch(InfraException e){
            GuiVeiculo.saidaDados(e.getMessage() );
        }
    }
    
}
