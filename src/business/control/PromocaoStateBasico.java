package business.control;

import java.io.Serializable;

/**
 *
 * @author Jorcyane
 */
public class PromocaoStateBasico implements PromocaoStateIF,Serializable{
//Padrão State
    @Override
    public PromocaoStateIF alteraState() {
        return new PromocaoStatePacote();
    }
    
    @Override
    public String imprimeState(){
        return "Promoção Básica";
    }
    
}
