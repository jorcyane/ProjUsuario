package business.control;

/**
 *
 * @author Jorcyane
 */
public abstract class GerenciaRelatoriosTemplate {
    /*usando o padrão template method */
    public String montarrelatio(){
        String relatorio="";
        relatorio+="Relatórios da Locadora de Veículos:\n\n";
        relatorio+=cabecalhoespecifico();
        relatorio+="\n\n";
        relatorio+=corpoespecifico();
        return relatorio;
    }
    
    public abstract String cabecalhoespecifico();
    public abstract String corpoespecifico();
}
