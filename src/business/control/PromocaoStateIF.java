package business.control;

/**
 *
 * @author Jorcyane
 */
public interface PromocaoStateIF {
    //Padrão State
    public PromocaoStateIF alteraState();
    public String imprimeState();
}
