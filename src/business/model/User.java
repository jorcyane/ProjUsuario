package business.model;
import java.io.Serializable;
/**
 *
 * @author Jorcyane
 */
public class User implements UserIF, Serializable{
    private String login;
    private String senha;
    
    public User(String login,String senha){
        this.login = login;
        this.senha = senha;
    }
    
    @Override
    public String toString(){
        return "Login: "+login+" ||Senha: "+senha;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getSenha() {
        return senha;
    }

    @Override
    public void setSenha(String senha) {
        this.senha = senha;
    }
}
