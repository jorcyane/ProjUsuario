package business.model;

import exception.LoginInvalidException;
import exception.PasswordInvalidException;

/**
 *
 * @author Jorcyane
 */
public class ValidacaoUser {
    public static void validateName(String nome) throws LoginInvalidException   {
		
	if(nome.length() > 20) 
            throw new LoginInvalidException("Login com mais de 20 caracteres!\nTente novamente!");
	else if(nome.length() == 0)
            throw new LoginInvalidException("Login vazio!\nTente novamente!");
	else if(nome.matches(".*\\d.*"))
             throw new LoginInvalidException("Login não pode conter numeros!\nTente novamente!");;
    }
	
	public static void validatePassword(String pass) throws PasswordInvalidException   {
            if(pass.length() > 12) 
                throw new PasswordInvalidException("Senha não pode possuir mais de 12 caracteres!\nTente novamente!");
            else if(pass.length() < 8)
                throw new PasswordInvalidException("Senha não pode possuir menos de 8 caracteres!\nTente novamente!");
            else if(!pass.matches(".*\\d.*") || !pass.matches(".*\\c.*")) 
                throw new PasswordInvalidException("Senha deve possuir caracteres e numeros!\nTente novamente!");
            else if (countNumbers(pass) < 2)
                throw new PasswordInvalidException("Senha deve pelo menos 2 numeros!\nTente novamente!");
	}
	
	private static int countNumbers(String s){
	    int count = 0;
	    for (int i = 0, len = s.length(); i < len; i++) {
	        if (Character.isDigit(s.charAt(i))) {
	            count++;
	        }
	    }
	    return count;
	}
}
