package business.model;
/**
 *
 * @author aluno
 */
public interface UserIF {
      
    public String getLogin();
    public void setLogin(String login);
    public String getSenha();
    public void setSenha(String senha);
    
}
