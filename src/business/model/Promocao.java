package business.model;

import business.control.PromocaoStateBasico;
import business.control.PromocaoStateIF;
import java.io.Serializable;

/**
 *
 * @author Jorcyane
 */
public class Promocao implements Serializable{
    private String nome;
    private String descricao;
    private PromocaoStateIF tipo;
    
    public Promocao(String nome,String descricao){
        this.nome = nome;
        this.descricao = descricao;
        //Usando o padrão state
        this.tipo = new PromocaoStateBasico();
    }
    
    @Override
    public String toString(){
        return "Nome da Promocao: "+nome+" ||Descricao: "+descricao+"||Tipo: "+tipo.imprimeState();
    }

    public String getNomePromocao() {
        return nome;
    }

    public void setNomePromocao(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public void alteraState(){
        this.tipo=tipo.alteraState();
    }
    
}
