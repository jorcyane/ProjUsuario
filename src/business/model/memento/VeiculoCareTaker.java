package business.model.memento;

import java.util.ArrayList;

/**
 *
 * @author Jorcyane
 */
public class VeiculoCareTaker {
//Padrão memento    
    
    protected MementoVeiculo estado;
    
    public VeiculoCareTaker(){
        estado = null;
    }
    
    public void adicionarMemento(MementoVeiculo memento) {
        estado = memento;
    }
    
    public MementoVeiculo getUltimoEstadoSalvo() {
        MementoVeiculo estado_old = estado;
        
        estado = null;
        return estado_old;
    }
}
