package business.model.memento;

/**
 *
 * @author Jorcyane
 */
public class MementoVeiculo {
// Usa o padrão Memento    

    protected String nomeMemento;
    protected String descricaoMemento;
    protected String marcaMemento;
    protected String modeloMemento;
    protected boolean disponivelMemento;
    
    public MementoVeiculo(String nomeMemento,String descricaoMemento,String marcaMemento,String modeloMemento,boolean disponivelMemento){
        this.nomeMemento = nomeMemento;
        this.descricaoMemento = descricaoMemento;
        this.marcaMemento = marcaMemento;
        this.modeloMemento = modeloMemento;
        this.disponivelMemento = disponivelMemento;
        
    }
    
    public String getNome(){
        return nomeMemento;
    }
    
    public String getDescricao(){
        return descricaoMemento;
    }
    
    public String getMarca(){
        return marcaMemento;
    }
    
    public String getModelo(){
        return modeloMemento;
    }
    
    public boolean getDisponivel(){
        return disponivelMemento;
    }
    
    public String toString(){
        return "memento :"+nomeMemento;
    }
}
