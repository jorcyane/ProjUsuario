package business.model;
import java.io.Serializable;

/**
 *
 * @author Arthuci e Jorcyane
 */
public class Veiculo implements Serializable{
    private String nome;
    private String descricao;
    private String marca;
    private String modelo;
    private boolean disponivel;
    
    public Veiculo(String nome,String descricao,String marca,String modelo){
        this.nome = nome;
        this.descricao = descricao;
        this.marca = marca;
        this.modelo = modelo;
        this.disponivel = true;
    }
    
    @Override
    public String toString(){
        String msg = "Veículo: "+nome+" ||Descricao: "+descricao+" || Marca: "+marca+" ||Modelo: "+modelo;
        if (disponivel){
            msg +=" ----Disponível para locação.";
        }else{
             msg +=" ----Locado.";
        }
        return msg;
    }

    public String getNomeCarro() {
        return nome;
    }

    public void setNomeCarro(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public void setDisponivel(boolean disponivel){
        this.disponivel = disponivel;
    }
    
    public boolean getDisponivel(){
        return disponivel;
    }
    
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
