/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.model;

/**
 *
 * @author Arthuci e Jorcyane
 */
public class UserAdm extends User{
    public UserAdm(String login,String senha){
        super(login,senha);
    }
    public String toString(){
        return "[Login: "+super.getLogin()+" ||Senha: "+super.getSenha()+"] - [Perfil de Gerente";
    }
}
