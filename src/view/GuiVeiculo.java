package view;

import business.control.ControlVeiculos;
import business.control.VeiculosFacadeCommmand;
import exception.InfraException;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorcyane
 */
public class GuiVeiculo {
    public static void menuVeiculo () {
         //usando o padrão singleton
        ControlVeiculos controlveiculos = ControlVeiculos.getInstanciaCV();
        String choice,nome,descricao,marca,modelo,msg;
        VeiculosFacadeCommmand facadeCommand=new VeiculosFacadeCommmand();
        boolean locar;
        int resposta; 
        choice= showMenu();
        while(!choice.equals("9")){
            
            switch(choice){
                case "1":
                    nome = JOptionPane.showInputDialog("Nome do veiculo:");
                    descricao = JOptionPane.showInputDialog("Descricao do veículo:");
                    marca = JOptionPane.showInputDialog("Marca do veículo:");
                    modelo = JOptionPane.showInputDialog("Modelo do veículo:");
                    
                        try {
                            controlveiculos.addVeiculo(nome, descricao, marca, modelo);
                            JOptionPane.showMessageDialog(null, "Veículo adicionado com sucesso!");
                        } catch(InfraException e){
                            JOptionPane.showMessageDialog(null, e.getMessage() );
                        }
                        choice=showMenu();
                break;
                case "2":
                    msg=controlveiculos.getAllVeiculosString();
                    JOptionPane.showMessageDialog(null, msg);
                    
                    choice=showMenu();
                break;
                case "3":
                    nome = JOptionPane.showInputDialog("Nome do veículo:");
                    try{
                        if (controlveiculos.removeVeiculo(nome)){
                            JOptionPane.showMessageDialog(null, "Veículo excluído com sucesso!" );
                        }else{
                            JOptionPane.showMessageDialog(null, "Veículo não encontrado!" );
                        };
                    }catch (InfraException e){
                        JOptionPane.showMessageDialog(null, "Falha na exclusão do veículo. \n"+e.getMessage() );
                    }
                    choice=showMenu();
                break;
                case "4":
                    nome = JOptionPane.showInputDialog("Nome do veículo:");
                    try{
                        locar = controlveiculos.locar(nome);
                        if (locar){
                            msg = "Veículo locado com sucesso";
                        }else {
                            msg = "Erro na locação, verifique nome e disponibilidade do veículo.";
                        }
                        JOptionPane.showMessageDialog(null, msg );
                    }catch(InfraException e){
                        JOptionPane.showMessageDialog(null, "Falha na locação do veículo. \n"+e.getMessage() );
                    }
                    choice=showMenu();
                    break;
                case "5":nome = JOptionPane.showInputDialog("Nome do veículo:"); try{
                        msg = controlveiculos.devolver(nome);
                        JOptionPane.showMessageDialog(null, msg );
                    }catch(InfraException e){
                        JOptionPane.showMessageDialog(null, "Falha na locação do veículo. \n"+e.getMessage() );
                    }choice=showMenu();
                break;                   
                case "6":
                    facadeCommand.buscaVeiculos();
                    choice=showMenu();
                break;        
                case "7":
                    facadeCommand.atualizaVeiculos();
                    choice=showMenu();
                break;  
                case "8":
                    facadeCommand.desfazerAtualizacao();
                    choice=showMenu();
                break;
                case "9":
                    //Opção para sair do sistema
                break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida!");
                    choice=showMenu();
                break;
            }
        }
    }   
    
    public static String showMenu() {
	String choice;
        return choice = JOptionPane.showInputDialog("Bem vindo!\nEscolha a opcao desejada:\n1-Cadastrar Veículo\n2-Listar Veículos\n3-Excluir Veículos\n4-Locar Veículo\n5-Devolver Veículo\n6-Buscar Veículos\n7-Atualizar Veiculos\n8-Desfazer Atualizaão\n9-Voltar para o menu principal","Sua opcao:");		
    }
    
    public static String entradaDados(String msg){
        return JOptionPane.showInputDialog(msg);
    }
    
    public static void saidaDados(String msg){
        JOptionPane.showMessageDialog(null, msg);
    }
    
    public static String saidaOptions(String msg){
        int resposta = JOptionPane.showConfirmDialog(null, msg);
        if (resposta == JOptionPane.YES_OPTION){
            return "YES";
        }else{ 
            if (resposta == JOptionPane.NO_OPTION){
                return "NO";
            }
                
        }
        return null;
    }
    
}
