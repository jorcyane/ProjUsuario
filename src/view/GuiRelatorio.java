package view;

import business.control.RelatorioPromocoes;
import business.control.RelatorioUsuario;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorcyane
 */
public class GuiRelatorio {
    public static void menuRelatorio () {
        String choice;
        String relatorio;
        choice= showMenu();
        while(!choice.equals("3")){
            
            switch(choice){
                case "1":
                        RelatorioUsuario relatoriouser=new RelatorioUsuario();
                        relatorio = relatoriouser.montarrelatio();
                        JOptionPane.showMessageDialog(null, relatorio);
                        choice=showMenu();
                break;
                case "2":
                    RelatorioPromocoes relatoriopromocoes = new RelatorioPromocoes();
                    relatorio = relatoriopromocoes.montarrelatio();
                    JOptionPane.showMessageDialog(null, relatorio);
                    choice=showMenu();
                break;
                case "3":
                    //Opção para sair do sistema
                break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida!");
                    choice=showMenu();
                break;
            }
        }
    }   
    
    public static String showMenu() {
	String choice;
        return choice = JOptionPane.showInputDialog("Bem vindo!\nEscolha a opcao desejada:\n1-Relatório de Usuários\n2-Relatório de Promoções\n3-Voltar para o menu principal","Sua opcao:");		
    }
}
