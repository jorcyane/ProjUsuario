package view;

import business.control.VeiculosDisponiveisFacade;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorcyane
 */
public class GuiMain {
    
    public static void main (String[] args) {
        String choice;
         
        choice= showMenu();
        while(!choice.equals("6")){
            
            switch(choice){
                case "1":
                    GuiUser.menuUser();
                    choice=showMenu();
                break;
                case "2":
                    GuiPromocao.menuPromocao();
                    choice=showMenu();
                break;
                case "3":
                    GuiRelatorio.menuRelatorio();
                    choice=showMenu();
                break;
                case "4":
                    GuiVeiculo.menuVeiculo();
                    choice=showMenu();
                    break;
                case "5":
                    /*usando o padrão Facade */
                    JOptionPane.showMessageDialog(null,VeiculosDisponiveisFacade.listarVeiculosDisponiveis());
                    choice=showMenu();
                break;                
                case "6":
                    //Opção para sair do sistema
                break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida!");
                    choice=showMenu();
                break;
            }
        }
    }   
    
    public static String showMenu() {
	String choice;
        return choice = JOptionPane.showInputDialog("Bem vindo!\nEscolha a opcao desejada:\n1-Cadastro de Usuario\n2-Cadastro e Divulgação de promoções\n3-Relatórios\n4-Gerência dos Veículos\n5-Listar Veículos Disponívies\n6-Sair","Sua opcao:");		
    }
    
}
