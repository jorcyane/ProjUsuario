package view;

import business.control.ControlUser;
import exception.InfraException;
import exception.LoginInvalidException;
import exception.PasswordInvalidException;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorcyane
 */
public class GuiUser {
    
    public static void menuUser () {
        ControlUser controluser = new ControlUser();
        String choice,name,pass;
        int resposta;
          
        choice= showMenu();
        while(!choice.equals("4")){
            
            switch(choice){
                case "1":
                    name = JOptionPane.showInputDialog("Nome do usuario:");
                    pass = JOptionPane.showInputDialog("Senha do usuario:");
                    resposta = JOptionPane.showConfirmDialog(null, "Perfil de Gerente?");
                    
                        try {
                            if (resposta == JOptionPane.YES_OPTION) {
                                controluser.addUserAdm(name, pass);}
                            else {
                                 controluser.addUser(name, pass);
                            }
                            JOptionPane.showMessageDialog(null, "Usuario adicionado com sucesso!");
                        } catch (LoginInvalidException e) {
                            JOptionPane.showMessageDialog(null, e.getMessage() );
                        } catch (PasswordInvalidException e) {
                            JOptionPane.showMessageDialog(null, e.getMessage() );
                        } catch(InfraException e){
                            JOptionPane.showMessageDialog(null, e.getMessage() );
                        }
                        choice=showMenu();
                break;
                case "2":
                    String msg=controluser.getAllClientsString();
                    JOptionPane.showMessageDialog(null, msg);
                    
                    choice=showMenu();
                break;
                case "3":
                    name = JOptionPane.showInputDialog("Nome do usuario:");
                    try{
                        if (controluser.removeUsuario(name)){
                            JOptionPane.showMessageDialog(null, "Usuário excluído com sucesso!" );
                        }else{
                            JOptionPane.showMessageDialog(null, "Usuário não encontrado!" );
                        };
                    }catch (InfraException e){
                        JOptionPane.showMessageDialog(null, "Falha na exclusão do usuário. \n"+e.getMessage() );
                    }
                    choice=showMenu();
                break;
                case "4":
                    //Opção para sair do sistema
                break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida!");
                    choice=showMenu();
                break;
            }
        }
    }   
    
    public static String showMenu() {
	String choice;
        return choice = JOptionPane.showInputDialog("Bem vindo!\nEscolha a opcao desejada:\n1-Cadastrar Usuario\n2-Listar Usuarios\n3-Excluir Usuario\n4-Voltar para o menu principal","Sua opcao:");		
    }
    
}
