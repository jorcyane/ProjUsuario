package view;

import boundary.IntegracaoRedesSociaisEmail;
import business.control.GerenciaPromocoes;
import exception.InfraException;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorcyane
 */
public class GuiPromocao {
    public static void menuPromocao () {
        GerenciaPromocoes controlpromocao = new GerenciaPromocoes();
        String choice,nome,descricao;
        int resposta; 
        choice= showMenu();
        while(!choice.equals("5")){
            
            switch(choice){
                case "1":
                    nome = JOptionPane.showInputDialog("Nome da promoão:");
                    descricao = JOptionPane.showInputDialog("Descricao da Promocao:");
                        try {
                            controlpromocao.addPromocao(nome, descricao);
                            JOptionPane.showMessageDialog(null, "Promoção adicionada com sucesso!");
                            resposta = JOptionPane.showConfirmDialog(null, "Divulgar a promoção?");
                            if (resposta == JOptionPane.YES_OPTION){
                                IntegracaoRedesSociaisEmail.divulgar(controlpromocao.buscaPromocao(nome));
                            }
                        } catch(InfraException e){
                            JOptionPane.showMessageDialog(null, e.getMessage() );
                        }
                        choice=showMenu();
                break;
                case "2":
                    String msg=controlpromocao.getAllPromocoesString();
                    JOptionPane.showMessageDialog(null, msg);
                    
                    choice=showMenu();
                break;
                case "3":
                    nome = JOptionPane.showInputDialog("Nome da promocao:");
                    try{
                        if (controlpromocao.removePromocao(nome)){
                            JOptionPane.showMessageDialog(null, "Promoção excluída com sucesso!" );
                        }else{
                            JOptionPane.showMessageDialog(null, "Promoção não encontrada!" );
                        };
                    }catch (InfraException e){
                        JOptionPane.showMessageDialog(null, "Falha na exclusão da promocao. \n"+e.getMessage() );
                    }
                    choice=showMenu();
                break;
                case "4":
                    nome = JOptionPane.showInputDialog("Nome da promocao:");
                    try{
                        if (controlpromocao.alteraState(nome)){
                            JOptionPane.showMessageDialog(null, "Promoção alterada com sucesso!" );
                        }else{
                            JOptionPane.showMessageDialog(null, "Promoção não encontrada!" );
                        };
                    }catch (InfraException e){
                        JOptionPane.showMessageDialog(null, "Falha na alteração da promocao. \n"+e.getMessage() );
                    }
                    choice=showMenu();
                break;
                case "5":
                    //Opção para sair do sistema
                break;    
                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida!");
                    choice=showMenu();
                break;
            }
        }
    }   
    
    public static String showMenu() {
	String choice;
        return choice = JOptionPane.showInputDialog("Bem vindo!\nEscolha a opcao desejada:\n1-Cadastrar Promoção\n2-Listar Promoções\n3-Excluir Promoções\n4-Alterar Tipo de Promoção\n5-Voltar para o menu principal","Sua opcao:");		
    }
}
