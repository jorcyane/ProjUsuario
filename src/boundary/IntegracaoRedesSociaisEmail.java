package boundary;

import business.model.Promocao;
/**
 *
 * @author Jorcyane
 */
public class IntegracaoRedesSociaisEmail{
    public static void divulgar(Promocao promocao){
        /*usando o padrão Adapter */
        AdapterIntegracaoRedesSociaisEmail adaptador = new AdapterIntegracaoRedesSociaisEmail();
        adaptador.divulgar(promocao.getNomePromocao());
    }

}
