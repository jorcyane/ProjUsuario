package boundary;

/**
 *
 * @author Jorcyane
 */
public interface IntegracaoDivulgacaoIF {
    public void divulgar(String descricao);
}
