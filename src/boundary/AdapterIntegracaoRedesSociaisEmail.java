package boundary;

import business.control.ControlUser;
import java.util.ArrayList;
import static mock.SimulacaoRedesSociais.publicarRedesSociais;

/**
 *
 * @author Jorcyane
 */
public class AdapterIntegracaoRedesSociaisEmail implements IntegracaoDivulgacaoIF{
    /*usando o padrão Adapter */
    @Override
    public void divulgar(String promocao){
        ArrayList<String> nomeusuarios;
        ControlUser controluser = new ControlUser();
        nomeusuarios = controluser.getListAllNames();
        for (String nome:nomeusuarios){
            publicarRedesSociais(promocao,nome);
        }
    }
}
