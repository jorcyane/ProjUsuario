package infra;

import business.model.Promocao;
import exception.InfraException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Jorcyane
 */
public class PromocaoFile extends FileIF{
    public static Logger logger = Logger.getLogger(PromocaoFile.class.getName());

    public PromocaoFile() {
	inicializaFile("relatorioLogPromocao.txt");		
    }
    
    public void save(Map<String,Promocao> promocoes) throws InfraException{
        File file = new File("promocao.bin");
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(promocoes);
            out.close();
            
        } catch (FileNotFoundException ex) {
            throw new InfraException("Arquivo não encontrado, contacte o admin ou tente mais tarde");
        } catch (IOException ex){
            throw new InfraException("Contacte o admin ou tente mais tarde");

        }
    }
    
    public Map<String,Promocao> load() throws InfraException{
    	Map<String,Promocao> promocoes = new HashMap<>();
    	File file = new File("promocao.bin");
        ObjectInputStream objInput = null;
        InputStream in = null;
        if(!file.exists()){
        	save(promocoes);
        }
        try {
            in = new FileInputStream(file);
            //Recupera a lista
            objInput = new ObjectInputStream(in);
            promocoes = (Map<String,Promocao>) objInput.readObject();
     

            return promocoes;
        
        } catch (NullPointerException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");
           
        } catch (IOException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");
        } catch (ClassNotFoundException ex) {
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");      
        }
        finally {
            try {
		objInput.close();
		in.close();
            } catch (IOException e) {
		logger.severe(e.getMessage());
            }
  			
        }
    }
    
    public void remove(Map<String, Promocao> promocoes)throws InfraException{
        File file = new File("promocao.bin");
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.reset();
            out.close();
            save(promocoes);
        
        } catch (FileNotFoundException ex) {
            throw new InfraException("Arquivo não encontrado, contacte o admin ou tente mais tarde");
        } catch (IOException ex){
            throw new InfraException("Contacte o admin ou tente mais tarde");

        }
    
    }
}
