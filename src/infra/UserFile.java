package infra;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import business.model.User;
import exception.InfraException;
import java.util.HashMap;
import java.util.Map;


public class UserFile extends FileIF{
	
    public static Logger logger = Logger.getLogger(UserFile.class.getName());

    public UserFile() {
        inicializaFile("relatorioLogUser.txt");
    }
    
    public void save(Map<String,User> users) throws InfraException{
        File file = new File("user.bin");
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(users);
            out.close();
            
        } catch (FileNotFoundException ex) {
            throw new InfraException("Arquivo não encontrado, contacte o admin ou tente mais tarde");
        } catch (IOException ex){
            throw new InfraException("Contacte o admin ou tente mais tarde");

        }
    }
    
    public Map<String,User> load() throws InfraException{
    	Map<String,User> users = new HashMap<>();
    	File file = new File("user.bin");
        ObjectInputStream objInput = null;
        InputStream in = null;
        if(!file.exists()){
        	save(users);
        }
        try {
            in = new FileInputStream(file);
            //Recupera a lista
            objInput = new ObjectInputStream(in);
            users = (Map<String,User>) objInput.readObject();

          

            return users;
        
        } catch (NullPointerException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");
           
        } catch (IOException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");
        } catch (ClassNotFoundException ex) {
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");      
        }
        finally {
            try {
		objInput.close();
		in.close();
            } catch (IOException e) {
		logger.severe(e.getMessage());
            }
  			
        }
    }
    
    public void remove(Map<String, User> usuarios)throws InfraException{
        File file = new File("user.bin");
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.reset();
            out.close();
            save(usuarios);
        
        } catch (FileNotFoundException ex) {
            throw new InfraException("Arquivo não encontrado, contacte o admin ou tente mais tarde");
        } catch (IOException ex){
            throw new InfraException("Contacte o admin ou tente mais tarde");

        }
    
    }
}