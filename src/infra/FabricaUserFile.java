package infra;

/**
 *
 * @author  Jorcyane
 */
public class FabricaUserFile implements FabricaFileIF{
/*usando o padrão factory method */
    @Override
    public FileIF criarFile() {
        return new UserFile();
    }
    
}
