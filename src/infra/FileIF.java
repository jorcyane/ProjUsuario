package infra;

import static infra.PromocaoFile.logger;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;

/**
 *
 * @author Jorcyane
 */
public abstract class FileIF {
    
    public static void inicializaFile(String nomearquivo){
        try {
           
            Handler hdConsole = new ConsoleHandler();
            Handler hdArquivo = new FileHandler(nomearquivo);

            hdConsole.setLevel(Level.OFF);
            hdArquivo.setLevel(Level.OFF);

            PromocaoFile.logger.addHandler(hdConsole);
            PromocaoFile.logger.addHandler(hdArquivo);

            PromocaoFile.logger.setUseParentHandlers(false);


        } catch (IOException ex) {
            logger.severe("ocorreu um erro no arquivo durante a execução do programa");
        }
    }
}
