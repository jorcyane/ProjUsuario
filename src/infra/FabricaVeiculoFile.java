package infra;

/**
 *
 * @author Jorcyane
 */
public class FabricaVeiculoFile implements FabricaFileIF{
/*usando o padrão factory method */
    @Override
    public FileIF criarFile() {
        return new VeiculoFile(); 
    }
    
}
