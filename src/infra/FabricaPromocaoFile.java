package infra;

/**
 *
 * @author Jorcyane
 */
public class FabricaPromocaoFile implements FabricaFileIF{
/*usando o padrão factory method */
    @Override
    public FileIF criarFile() {
        return new PromocaoFile();
    }
    
}
