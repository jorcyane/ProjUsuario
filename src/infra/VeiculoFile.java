
package infra;

import business.model.Veiculo;
import exception.InfraException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Arthuci e Jorcyane
 */
public class VeiculoFile extends FileIF{
    public static Logger logger = Logger.getLogger(VeiculoFile.class.getName());

    public VeiculoFile() {
	inicializaFile("relatorioLogVeiculo.txt");		
    }
    
    public void save(Map<String,Veiculo> veiculos) throws InfraException{
        File file = new File("veiculo.bin");
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(veiculos);
            out.close();
            
        } catch (FileNotFoundException ex) {
            throw new InfraException("Arquivo não encontrado, contacte o admin ou tente mais tarde");
        } catch (IOException ex){
            throw new InfraException("Contacte o admin ou tente mais tarde");

        }
    }
    
    public Map<String,Veiculo> load() throws InfraException{
    	Map<String,Veiculo> veiculos = new HashMap<>();
    	File file = new File("veiculo.bin");
        ObjectInputStream objInput = null;
        InputStream in = null;
        if(!file.exists()){
        	save(veiculos);
        }
        try {
            in = new FileInputStream(file);
            //Recupera a lista
            objInput = new ObjectInputStream(in);
            veiculos = (Map<String,Veiculo>) objInput.readObject();
     

            return veiculos;
        
        } catch (NullPointerException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");
           
        } catch (IOException ex){
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");
        } catch (ClassNotFoundException ex) {
            logger.config(ex.getMessage());
            throw new InfraException("Erro de persistencia, contacte o admin ou tente mais tarde");      
        }
        finally {
            try {
		objInput.close();
		in.close();
            } catch (IOException e) {
		logger.severe(e.getMessage());
            }
  			
        }
    }
    
    public void remove(Map<String, Veiculo> veiculos)throws InfraException{
        File file = new File("veiculo.bin");
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.reset();
            out.close();
            save(veiculos);
        
        } catch (FileNotFoundException ex) {
            throw new InfraException("Arquivo não encontrado, contacte o admin ou tente mais tarde");
        } catch (IOException ex){
            throw new InfraException("Contacte o admin ou tente mais tarde");

        }
    
    }
    public void edita(Map<String, Veiculo> veiculos)throws InfraException{
        File file = new File("veiculo.bin");
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.reset();
            out.close();
            save(veiculos);
        
        } catch (FileNotFoundException ex) {
            throw new InfraException("Arquivo não encontrado, contacte o admin ou tente mais tarde");
        } catch (IOException ex){
            throw new InfraException("Contacte o admin ou tente mais tarde");

        }
    
    }
}
